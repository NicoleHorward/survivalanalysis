
## Survival Analysis in R 

This repository contains examples of different survival models in R. 

---

## Rats Dataset Analysis

The R notebook "RatsDatasetAnalysis" takes a closer look at the Rats dataset that we'll be using. 

---

## Rats Survival Models

The R notebook "RatsSurvivalModels" generates four different survival models using the Rats dataset: 

1. Linear

2. Log-normal

3. Kaplan-Meier

![picture](img/KMRats.png)

4. Weibull

![picture](img/WBRats.png)

---

## Cox Model

The R notebook "CoxModel" takes a closer look at the Cox model and plots the proportional hazards of the Rats dataset.

![picture](img/HRRats.png)

---

## SurvivalHeart

The R notebook "SurvivalHeart" analysis, alters and plots different survival curves for the Stanford2 dataset about heart transplants. 

---

## Sources

Sources:
Survival Analysis in R, DataCamp, via https://www.datacamp.com/courses/survival-analysis-in-r
Survival Analysis Basics, STHDA, via http://www.sthda.com/english/wiki/survival-analysis-basics 
Cox Proportional Hazards, Wikipedia, via https://en.wikipedia.org/wiki/Survival_analysis#Cox_proportional_hazards_(PH)_regression_analysis
Survival Analysis in R for beginners, Daniel Schuette, DataCamp, via https://www.datacamp.com/community/tutorials/survival-analysis-R
